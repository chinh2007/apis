namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<WebApplication1.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WebApplication1.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Categories.AddOrUpdate(x => x.ID, 
               new Models.Category{ ID = 1, Name = "Mobile" },
               new Models.Category { ID = 2, Name = "Tablet" },
               new Models.Category { ID = 3, Name = "Smart watch" }
               );

            context.Products.AddOrUpdate(x => x.ID,
                new Models.Product
                {
                    ID = 1,
                    Name = "Product 1",
                    CategoryID = 1,
                    Price = 9.99M,
                },
                new Models.Product
                {
                    ID = 2,
                    Name = "Product 2",
                    CategoryID = 1,
                    Price = 12.95M,
                },
                new Models.Product
                {
                    ID = 3,
                    Name = "Product 3",
                    CategoryID = 2,
                    Price = 15,
                },
                new Models.Product
                {
                    ID = 4,
                    Name = "Product 4",
                    CategoryID = 3,
                    Price = 10,
                }

                );
        }
    }
}
